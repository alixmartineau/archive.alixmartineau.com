import uuid

from django.contrib.auth import get_user_model

User = get_user_model()


class UserMixin:
    def any_user(self, username=""):
        username = username or str(uuid.uuid4())
        try:
            return User.objects.get(username=username)
        except User.DoesNotExist:
            return User.objects.create_user(username=username)
