from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import QuerySet
from django.views.generic import ListView

from archive.bookmarks.models import Bookmark

__all__ = ["HomeView"]


class HomeView(LoginRequiredMixin, ListView):
    template_name = "www/home.html"

    def get_queryset(self) -> QuerySet:
        return Bookmark.objects.filter(user=self.request.user).order_by("-created")
