from dataclasses import dataclass, field

from bs4 import BeautifulSoup
from django.http import HttpResponse
from django.test import Client
from django.urls import reverse

from archive.tests.mixins import UserMixin


class AuthenticatedClient(Client):
    def __init__(self, *args, user=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = user or UserMixin().any_user()
        self.force_login(self.user)


class Redirect(Exception):
    ...


@dataclass
class Page:
    response: HttpResponse
    soup: BeautifulSoup = field(init=False)

    def __post_init__(self):
        self.soup = BeautifulSoup(self.response.content, "html.parser")


class HomePage(Page):
    @classmethod
    def get(cls, client=None):
        client = client or AuthenticatedClient()
        resp = client.get(reverse("www:home"))
        if resp.status_code == 302:
            raise Redirect(resp)
        assert resp.status_code == 200
        return cls(resp)
