from django.test import Client, TestCase

from archive.bookmarks.tests.mixins import BookmarksMixin

from . import AuthenticatedClient, HomePage, Redirect


class HomeTests(TestCase, BookmarksMixin):

    client_class = AuthenticatedClient

    def test_home(self) -> None:
        HomePage.get()

    def test_home_unauthenticated(self) -> None:
        with self.assertRaises(Redirect):
            HomePage.get(client=Client())
