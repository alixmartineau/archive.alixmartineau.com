import uuid

from archive.bookmarks.models import Bookmark
from archive.tests.mixins import UserMixin


class BookmarksMixin:
    def any_bookmark(self, user=None):
        title = str(uuid.uuid4())
        url = f"https://{title}"
        user = user or UserMixin().any_user()
        return Bookmark.objects.create(title=title, url=url, user=user)
