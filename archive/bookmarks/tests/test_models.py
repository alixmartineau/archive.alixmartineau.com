from django.test import TestCase
from hamcrest import assert_that, is_

from .mixins import BookmarksMixin


class BookmarkTests(TestCase, BookmarksMixin):
    def test_bookmark_representation(self):
        bookmark = self.any_bookmark()
        assert_that(str(bookmark), is_(bookmark.title))
