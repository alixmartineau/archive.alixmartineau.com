from django.conf import settings
from django.db import models
from django.utils import timezone


class Bookmark(models.Model):
    title = models.CharField(max_length=1024)
    url = models.URLField()
    created = models.DateTimeField(default=timezone.now)

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("url", "user")

    def __str__(self):
        return self.title
