from django.http import HttpResponse
from django.views import View
from rest_framework import mixins, viewsets

from archive.api.authentication import DEFAULT_AUTHENTICATION_CLASSES, DEFAULT_PERMISSION_CLASSES
from archive.bookmarks.models import Bookmark

from .serializers import BookmarkSerializer


class PingView(View):
    def get(self, request):
        return HttpResponse()


class BookmarksViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Bookmark.objects.all()
    serializer_class = BookmarkSerializer
    authentication_classes = DEFAULT_AUTHENTICATION_CLASSES
    permission_classes = DEFAULT_PERMISSION_CLASSES

    def get_queryset(self):
        return Bookmark.objects.filter(user=self.request.user)
