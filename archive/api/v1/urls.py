from django.urls import path
from rest_framework import routers

from .views import BookmarksViewSet, PingView

app_name = "v1"

router = routers.SimpleRouter()
router.register(r"bookmarks", BookmarksViewSet, basename="bookmarks")

urlpatterns = router.urls + [
    path("ping/", PingView.as_view(), name="ping"),
]
