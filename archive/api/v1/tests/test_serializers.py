from django.test import TestCase
from hamcrest import assert_that, has_entries
from rest_framework.fields import DateTimeField

from archive.api.v1.serializers import BookmarkSerializer
from archive.bookmarks.tests.mixins import BookmarksMixin


class SerializerTests(TestCase, BookmarksMixin):
    def test_bookmark_serializer(self) -> None:
        bookmark = self.any_bookmark()
        data = BookmarkSerializer(bookmark).data
        assert_that(
            data,
            has_entries(
                {
                    "id": bookmark.pk,
                    "title": bookmark.title,
                    "url": bookmark.url,
                    "created": DateTimeField().to_representation(bookmark.created),
                }
            ),
        )
