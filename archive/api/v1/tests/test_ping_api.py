from django.test import TestCase
from django.urls import reverse
from hamcrest import assert_that, is_


class PingAPITests(TestCase):
    def when_i_ping(self):
        self.last_response = self.client.get(reverse("api:v1:ping"))

    def test_ping_api(self) -> None:
        self.when_i_ping()
        assert_that(self.last_response.status_code, is_(200))
