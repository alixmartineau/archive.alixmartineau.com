from django.test import Client, TestCase
from django.urls import reverse
from hamcrest import assert_that, has_length, is_

from archive.api.tests import TestClient
from archive.bookmarks.tests.mixins import BookmarksMixin


class ReadAPITests(TestCase, BookmarksMixin):
    client_class = TestClient

    def when_i_list_bookmarks(self):
        url = reverse("api:v1:bookmarks-list")
        self.last_response = self.client.get(url)

    def test_list_bookmarks(self) -> None:
        self.any_bookmark(user=self.client.user)
        self.when_i_list_bookmarks()
        assert_that(self.last_response.status_code, is_(200))
        assert_that(self.last_response.data, has_length(1))

    def test_list_bookmarks__unauthenticated(self) -> None:
        self.client = Client()
        self.when_i_list_bookmarks()
        assert_that(self.last_response.status_code, is_(401))
