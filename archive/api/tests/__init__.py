from typing import Optional

from django.contrib.auth.models import AbstractUser
from rest_framework.test import APIClient

from archive.tests.mixins import UserMixin


class TestClient(APIClient):
    def __init__(self, user: Optional[AbstractUser] = None):
        super().__init__()
        self.user = user or UserMixin().any_user()
        self.force_authenticate(self.user)
