from rest_framework import authentication, permissions

__all__ = ["DEFAULT_AUTHENTICATION_CLASSES", "DEFAULT_PERMISSION_CLASSES"]


DEFAULT_AUTHENTICATION_CLASSES = [
    authentication.TokenAuthentication,
]

DEFAULT_PERMISSION_CLASSES = [
    permissions.IsAuthenticated,
]
